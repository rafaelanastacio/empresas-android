package com.example.rafaelanastacioalves.moby.enterprise_detailing;


import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.rafaelanastacioalves.moby.BuildConfig;
import com.example.rafaelanastacioalves.moby.R;
import com.example.rafaelanastacioalves.moby.domain.model.EnterpriseDetail;
import com.example.rafaelanastacioalves.moby.domain.model.EnterpriseDetailResponse;
import com.example.rafaelanastacioalves.moby.domain.model.Resource;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import dagger.android.support.AndroidSupportInjection;
import dagger.android.support.DaggerFragment;

/**
 * A simple {@link Fragment} subclass.
 */
public class EnterpriseDetailsFragment extends DaggerFragment implements View.OnClickListener {

    public static String ENTERPRISE_ID;

    private LiveDataEnterpriseDetailsViewModel mLiveDataEnterpriseDetailsViewModel;

    @Inject
    EnterpriseDetailViewModelFactory enterpriseDetailViewModelFactory;

    @BindView(R.id.detail_entity_detail_name)
    TextView enterpriseShares;

    @BindView(R.id.trip_package_detail_imageview)
    ImageView tripPackageDetailImageview;

    @Override
    public void onAttach(Context context) {
        settupadagger();
        super.onAttach(context);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        subscribe();
    }

    private void settupadagger() {
        AndroidSupportInjection.inject(this);
    }

    private void subscribe() {
        String mPackageId = getArguments().getString(ENTERPRISE_ID);
        mLiveDataEnterpriseDetailsViewModel = ViewModelProviders.of(this, enterpriseDetailViewModelFactory).get(LiveDataEnterpriseDetailsViewModel.class);
        mLiveDataEnterpriseDetailsViewModel.getEntityDetails(mPackageId).observe(this, new Observer<Resource<com.example.rafaelanastacioalves.moby.domain.model.EnterpriseDetailResponse>>() {
            @Override
            public void onChanged(@Nullable Resource<EnterpriseDetailResponse> entityDetails) {
                if (entityDetails.data != null) {
                    switch (entityDetails.status) {
                        case SUCCESS:
                            setViewsWith(entityDetails.data.enterpriseDetail);
                            break;
                        case LOADING:
                            break;
                        case UNAUTHORIZED:
                            Toast.makeText(getActivity(), "Não autorizado", Toast.LENGTH_SHORT).show();
                            break;
                        case INTERNAL_SERVER_ERROR:
                            Toast.makeText(getActivity(), "Erro Interno", Toast.LENGTH_SHORT).show();
                            break;

                    }

                }
            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflateViews(inflater, container);
    }

    private View inflateViews(LayoutInflater inflater, ViewGroup container) {
        View rootView = inflater.inflate(R.layout.fragment_enterprise_detail_view, container, false);
        ButterKnife.bind(this, rootView);
        return rootView;
    }

    private void setupActionBarWithTitle(String title) {
        AppCompatActivity mActivity = (AppCompatActivity) getActivity();
        ActionBar actionBar = mActivity.getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setTitle(title);
        }
    }

    private void setViewsWith(EnterpriseDetail enterpriseDetail) {
        enterpriseShares.setText(enterpriseDetail.shares);
        setupActionBarWithTitle(enterpriseDetail.enterpriseName);
        if (enterpriseDetail.photo != null) {
            Picasso.get()
                    .load(BuildConfig.API_BASE_URL + enterpriseDetail.photo)
                    .into(tripPackageDetailImageview, new Callback() {
                        @Override
                        public void onSuccess() {
                            getActivity().supportStartPostponedEnterTransition();
                        }

                        @Override
                        public void onError(Exception e) {

                        }
                    });
        } else {
            getActivity().supportStartPostponedEnterTransition();
        }

    }

    @Override
    public void onDestroy() {
        super.onDestroy();

    }

    @Override
    public void onClick(View v) {
        Toast.makeText(getActivity(), "Comprado!", Toast.LENGTH_SHORT).show();
    }
}
