package com.example.rafaelanastacioalves.moby.repository.local.interceptor;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class AuthInterceptor implements Interceptor {

    private final String CLIENT_TOKEN = "client";
    private final String UID_TOKEN = "uid";
    private final String ACCESS_TOKEN = "access-token";

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request original = chain.request();

        AuthManager authMan = AuthManager.getInstance();
        // Request customization: add request headers

        Request.Builder requestBuilder = original.newBuilder()
                .addHeader("Content-Type", "application/json")
                .addHeader(ACCESS_TOKEN, authMan.accessToken)
                .addHeader(CLIENT_TOKEN, authMan.client )
                .addHeader(UID_TOKEN, authMan.uid );

        Request request = requestBuilder.build();
        Response response = chain.proceed(request);

        process(response);

        return response;


    }

    private void process(Response response) {
        AuthManager authMan = AuthManager.getInstance();

        authMan.accessToken = parse(response.header(ACCESS_TOKEN));
        authMan.uid = parse(response.header(UID_TOKEN));
        authMan.client = parse(response.header(CLIENT_TOKEN));

    }

    private String parse(String header) {
        return header != null? header : "";
    }
}
