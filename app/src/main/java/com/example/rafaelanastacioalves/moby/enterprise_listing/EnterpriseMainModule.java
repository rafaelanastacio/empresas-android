package com.example.rafaelanastacioalves.moby.enterprise_listing;

import com.example.rafaelanastacioalves.moby.domain.interactors.MainEntityListInteractor;

import dagger.Module;
import dagger.Provides;

@Module
public class EnterpriseMainModule {


    @Provides
    EnterpriseListingViewModelFactory projectViewModelFactory(MainEntityListInteractor mainEntityListinInteractor){
        return new EnterpriseListingViewModelFactory(mainEntityListinInteractor);
    }

}
