package com.example.rafaelanastacioalves.moby.enterprise_detailing;

import com.example.rafaelanastacioalves.moby.domain.interactors.EnterpriseDetailInteractor;

import dagger.Module;
import dagger.Provides;

@Module
public class EnterpriseDetailModule {

    @Provides
    EnterpriseDetailViewModelFactory buildEntityDetailViewModelFactory(EnterpriseDetailInteractor entityDetailingInteractor){
        return new EnterpriseDetailViewModelFactory(entityDetailingInteractor);
    }
}
