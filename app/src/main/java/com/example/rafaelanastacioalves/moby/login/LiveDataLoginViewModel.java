package com.example.rafaelanastacioalves.moby.login;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;

import com.example.rafaelanastacioalves.moby.domain.interactors.LoginInteractor;
import com.example.rafaelanastacioalves.moby.domain.model.LoginRequestBody;
import com.example.rafaelanastacioalves.moby.domain.model.LoginResponse;
import com.example.rafaelanastacioalves.moby.domain.model.Resource;

public class LiveDataLoginViewModel extends ViewModel {

    private LoginInteractor mMainEntityListinInteractor;
    private LiveData<Resource<LoginResponse>> mMainEntityList;


    public LiveDataLoginViewModel(LoginInteractor interactor){
        this.mMainEntityListinInteractor = interactor;
    }

    public LiveData<Resource<LoginResponse>> login(String email, String password) {
        mMainEntityList = mMainEntityListinInteractor.execute(new LoginRequestBody(
                email,
                password));
        return mMainEntityList;
    }

}
