package com.example.rafaelanastacioalves.moby.domain.interactors;

import android.arch.lifecycle.LiveData;

import com.example.rafaelanastacioalves.moby.domain.model.EnterpriseDetailResponse;
import com.example.rafaelanastacioalves.moby.domain.model.Resource;
import com.example.rafaelanastacioalves.moby.repository.AppRepository;

import javax.inject.Inject;

public class EnterpriseDetailInteractor implements Interactor<EnterpriseDetailInteractor.RequestValues> {

    private final AppRepository appRepository;

    @Inject
    EnterpriseDetailInteractor(AppRepository appRepository){
        this.appRepository = appRepository;
    }

    @Override
    public LiveData<Resource<EnterpriseDetailResponse>> execute(RequestValues requestValues) {
        LiveData<Resource<EnterpriseDetailResponse>> repositoryLiveData = appRepository.getEnterpriseDetails(requestValues.resquestId);
        // aqui podemos aplicar transformações baseadas em regras de negócio usando
        // Transformations. Ex.: Transformations.map()
        return repositoryLiveData ;

    }

    public static final class RequestValues implements Interactor.RequestValues {
        private final String resquestId;

        public RequestValues(String resquestId) {
            this.resquestId = resquestId;
        }


        public String getResquestId() {
            return resquestId;
        }
    }


}
