package com.example.rafaelanastacioalves.moby.enterprise_detailing;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;
import android.support.annotation.NonNull;

import com.example.rafaelanastacioalves.moby.domain.interactors.EnterpriseDetailInteractor;


class EnterpriseDetailViewModelFactory implements ViewModelProvider.Factory {

    private final EnterpriseDetailInteractor mInteractor;

    public EnterpriseDetailViewModelFactory(EnterpriseDetailInteractor interactor){
        this.mInteractor = interactor;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {

        if (modelClass.isAssignableFrom(LiveDataEnterpriseDetailsViewModel.class)){
            return (T) new LiveDataEnterpriseDetailsViewModel(mInteractor);
        }
        throw new IllegalArgumentException("Unknown ViewModel class");
    }


}
