package com.example.rafaelanastacioalves.moby.repository.http;

import com.example.rafaelanastacioalves.moby.domain.interactors.LoginInteractor.LoginRequest;
import com.example.rafaelanastacioalves.moby.domain.model.EnterpriseDetailResponse;
import com.example.rafaelanastacioalves.moby.domain.model.Enterprises;
import com.example.rafaelanastacioalves.moby.domain.model.LoginResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface APIClient {

    @GET("/api/v1/enterprises")
    Call<Enterprises> getEnterprises();

    @GET("/api/v1/enterprises")
    Call<Enterprises> getEnterprises(@Query("name") String name);

    @GET("/api/v1/enterprises/{enterpriseId}")
    Call<EnterpriseDetailResponse> getEntepriseDetail(@Path("enterpriseId") String id);

    @POST("/api/v1/users/auth/sign_in")
    Call<LoginResponse> login(@Body LoginRequest requestValues);
}
