package com.example.rafaelanastacioalves.moby.domain.interactors;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Transformations;

import com.example.rafaelanastacioalves.moby.domain.interactors.Interactor.RequestValues;
import com.example.rafaelanastacioalves.moby.domain.model.LoginRequestBody;
import com.example.rafaelanastacioalves.moby.domain.model.LoginResponse;
import com.example.rafaelanastacioalves.moby.domain.model.Resource;
import com.example.rafaelanastacioalves.moby.repository.AppRepository;
import com.google.gson.annotations.SerializedName;

import javax.inject.Inject;

public class LoginInteractor implements Interactor<LoginRequestBody> {

    private final AppRepository appRepository;

    @Inject
    LoginInteractor(AppRepository appRepository) {
        this.appRepository = appRepository;
    }

    @Override
    public LiveData<Resource<LoginResponse>> execute(LoginRequestBody requestValues) {
        MutableLiveData<Resource<LoginResponse>> repositoryLiveData = (MutableLiveData<Resource<LoginResponse>>) appRepository.login(
                requestValues
        );
        // aqui podemos aplicar transformações baseadas em regras de negócio usando
        // Transformations. Ex.: Transformations.map()
        Transformations.map(repositoryLiveData, resource -> {

            switch (resource.status) {
                case GENERIC_ERROR:
                    resource.message = "Main entity list error";
            }

            return resource;

        });
        return repositoryLiveData;
    }

    public static class LoginRequest implements RequestValues {

        @SerializedName("email")
        public String email;

        @SerializedName("password")
        public String password;

        public LoginRequest(String email, String password) {
            this.email = email;
            this.password = password;
        }
    }
}
