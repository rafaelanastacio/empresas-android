package com.example.rafaelanastacioalves.moby.enterprise_listing;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.example.rafaelanastacioalves.moby.R;
import com.example.rafaelanastacioalves.moby.domain.model.Enterprise;
import com.example.rafaelanastacioalves.moby.listeners.RecyclerViewClickListener;

import java.util.ArrayList;
import java.util.List;

public class EnterpriseAdapter extends RecyclerView.Adapter<EnterpriseViewHolder> {
    private RecyclerViewClickListener recyclerViewClickListener;
    private List<Enterprise> items = new ArrayList<>();

    private Context mContext;

    public EnterpriseAdapter(Context context) {
        mContext = context;
    }


    public void setRecyclerViewClickListener(RecyclerViewClickListener aRVC) {
        this.recyclerViewClickListener = aRVC;
    }

    public List<Enterprise> getItems() {
        return this.items;
    }

    public void setItems(List<Enterprise> items) {
        this.items = items;
        notifyDataSetChanged();


    }

    @Override
    public EnterpriseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new EnterpriseViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.detail_enterprise_viewholder, parent, false), recyclerViewClickListener);
    }



    @Override
    public void onBindViewHolder(EnterpriseViewHolder holder, int position) {
        Enterprise aRepoW = getItems().get(position);
        ((EnterpriseViewHolder) holder).bind(aRepoW, mContext);
    }

    @Override
    public int getItemCount() {
        if (getItems() != null){
            return getItems().size();
        }else{
            return 0;
        }
    }
}

