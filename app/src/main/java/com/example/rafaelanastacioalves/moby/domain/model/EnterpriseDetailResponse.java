package com.example.rafaelanastacioalves.moby.domain.model;

import com.google.gson.annotations.SerializedName;

public class EnterpriseDetailResponse {

    @SerializedName("enterprise")
    public EnterpriseDetail enterpriseDetail;
}
