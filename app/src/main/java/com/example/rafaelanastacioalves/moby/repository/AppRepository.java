package com.example.rafaelanastacioalves.moby.repository;

import android.arch.lifecycle.LiveData;

import com.example.rafaelanastacioalves.moby.domain.interactors.LoginInteractor;
import com.example.rafaelanastacioalves.moby.domain.model.EnterpriseDetailResponse;
import com.example.rafaelanastacioalves.moby.domain.model.Enterprises;
import com.example.rafaelanastacioalves.moby.domain.model.LoginResponse;
import com.example.rafaelanastacioalves.moby.domain.model.Resource;
import com.example.rafaelanastacioalves.moby.repository.http.APIClient;
import com.example.rafaelanastacioalves.moby.repository.http.ServiceGenerator;

import javax.inject.Singleton;

import retrofit2.Call;

@Singleton
public class AppRepository {

    public LiveData<Resource<Enterprises>> getEnterpriseList() {
        APIClient apiClient = ServiceGenerator.createService(APIClient.class);
        return new NetworkBoundSource<Enterprises, Enterprises>() {
            @Override
            protected void onFetchFailed() {

            }

            @Override
            protected Call<Enterprises> createCall() {
                return apiClient.getEnterprises();
            }
        }.asLiveData();
    }

    public LiveData<Resource<EnterpriseDetailResponse>> getEnterpriseDetails(String id) {
        APIClient apiClient = ServiceGenerator.createService(APIClient.class);
        return new NetworkBoundSource<EnterpriseDetailResponse, EnterpriseDetailResponse>() {
            @Override
            protected void onFetchFailed() {

            }

            @Override
            protected Call<EnterpriseDetailResponse> createCall() {
                return apiClient.getEntepriseDetail(id);
            }
        }.asLiveData();
    }

    public LiveData<Resource<LoginResponse>> login(LoginInteractor.LoginRequest requestValues) {
        APIClient apiClient = ServiceGenerator.createService(APIClient.class);
        return new NetworkBoundSource<LoginResponse, LoginResponse>() {
            @Override
            protected void onFetchFailed() {

            }

            @Override
            protected Call<LoginResponse> createCall() {
                return apiClient.login(requestValues);
            }
        }.asLiveData();
    }
}



