
# Desafio ioasys

![N|Solid](logo_ioasys.png)


## Descrição Geral

Desafio tecnico da empresa *ioasys*.

Listagem:

![tela de listagem](captures/Screenshot_1566062147.png | width=50)

Detalhes:

![tela de detalhes](captures/Screenshot_1566062199.png | width=50)

## Descrição técnica

Utilizei as seguintes bibliotecas:

- Retrofit: Para utilização de consultas de API Http; 

- Rx Android: Para chamadas http assícronas;

- Live Data: Para um padrão observável dos elementos da View que requerem dados externos;

- Dagger: Para injeção de dependencias;

- Picasso: Para renderização de imagens;


## Se eu tivesse mais tempo:

- Implementaria as telas mais próximas do desenhado

- Adicionaria os tratamentos de exceções de uma maneira mais "user friendly";

- Implementaria a busca de empresas (apenas listei);

- Faria os testes funcionarem (iniciei mas não tive tempo de deixar funcionando);

## Como executar:

- Sincronize o gradle;

- Garanta que o build variant do Android Studio seja "debug".

- Aperte o "play" no Android Studio usando um emulador ou device real.
