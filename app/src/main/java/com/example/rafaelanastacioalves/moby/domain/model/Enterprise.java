package com.example.rafaelanastacioalves.moby.domain.model;


import com.google.gson.annotations.SerializedName;

public class Enterprise {

    @SerializedName("id")
    public int id;

    @SerializedName("enterprise_name")
    public String enterpriseName;

    @SerializedName("photo")
    public String photo;
}
