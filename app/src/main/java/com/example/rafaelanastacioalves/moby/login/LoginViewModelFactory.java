package com.example.rafaelanastacioalves.moby.login;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;
import android.support.annotation.NonNull;

import com.example.rafaelanastacioalves.moby.domain.interactors.LoginInteractor;
import com.example.rafaelanastacioalves.moby.domain.interactors.MainEntityListInteractor;
import com.example.rafaelanastacioalves.moby.enterprise_listing.LiveDataMainEnterpriseListViewModel;


class LoginViewModelFactory implements ViewModelProvider.Factory {

    private final LoginInteractor mInteractor;

    public LoginViewModelFactory(LoginInteractor interactor){
        this.mInteractor = interactor;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {

        if (modelClass.isAssignableFrom(LiveDataLoginViewModel.class)){
            return (T) new LiveDataLoginViewModel(mInteractor);
        }
        throw new IllegalArgumentException("Unknown ViewModel class");
    }


}
