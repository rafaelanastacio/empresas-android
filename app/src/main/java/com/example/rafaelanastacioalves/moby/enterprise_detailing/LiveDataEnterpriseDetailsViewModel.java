package com.example.rafaelanastacioalves.moby.enterprise_detailing;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;

import com.example.rafaelanastacioalves.moby.domain.interactors.EnterpriseDetailInteractor;
import com.example.rafaelanastacioalves.moby.domain.model.EnterpriseDetailResponse;
import com.example.rafaelanastacioalves.moby.domain.model.Resource;


public class LiveDataEnterpriseDetailsViewModel extends ViewModel {

    private EnterpriseDetailInteractor mEnterpriseInteractor;
    private LiveData<Resource<EnterpriseDetailResponse>> mEntityDetails;

    public LiveDataEnterpriseDetailsViewModel(EnterpriseDetailInteractor mInteractor) {
        this.mEnterpriseInteractor = mInteractor;
    }

    public LiveData<Resource<EnterpriseDetailResponse>> getEntityDetails(String entepriseID) {
        EnterpriseDetailInteractor.RequestValues requestValues = new EnterpriseDetailInteractor.RequestValues(entepriseID);
        mEntityDetails = mEnterpriseInteractor.execute(requestValues);
        return mEntityDetails;
    }


}

