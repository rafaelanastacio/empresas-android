package com.example.rafaelanastacioalves.moby.domain.model;


import com.google.gson.annotations.SerializedName;

public class EnterpriseDetail {

    @SerializedName("id")
    public int id;

    @SerializedName("description")
    public String description;

    @SerializedName("enterprise_name")
    public String enterpriseName;

    @SerializedName("shares")
    public String shares;

    @SerializedName("photo")
    public String photo;







}
