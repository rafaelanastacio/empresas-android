package com.example.rafaelanastacioalves.moby.di;

import android.app.Application;

import com.example.rafaelanastacioalves.moby.application.EnterpriseApp;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.support.AndroidSupportInjectionModule;

@Singleton
@Component(modules = {AndroidSupportInjectionModule.class, LogingModule.class, EntityDetailingModule.class, MainEntityListingModule.class, ApplicationModule.class})
public interface MyApplicationComponent {

    @Component.Builder
    interface Builder {
        @BindsInstance
        Builder application(Application application);
        MyApplicationComponent build();
    }

    void inject(EnterpriseApp application);
}
