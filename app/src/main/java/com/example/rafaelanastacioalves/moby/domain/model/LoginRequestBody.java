package com.example.rafaelanastacioalves.moby.domain.model;

import com.example.rafaelanastacioalves.moby.domain.interactors.LoginInteractor;

public class LoginRequestBody extends LoginInteractor.LoginRequest {
    public LoginRequestBody(String email, String password) {
        super(email, password);
    }
}
