package com.example.rafaelanastacioalves.moby.di;

import com.example.rafaelanastacioalves.moby.enterprise_detailing.EnterpriseDetailActivity;
import com.example.rafaelanastacioalves.moby.enterprise_detailing.EnterpriseDetailModule;
import com.example.rafaelanastacioalves.moby.enterprise_detailing.EnterpriseDetailsFragment;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class EntityDetailingModule {

    @ContributesAndroidInjector
    abstract EnterpriseDetailActivity bindEntityDetailActivity();


    @ContributesAndroidInjector(modules = EnterpriseDetailModule.class)
    abstract EnterpriseDetailsFragment bindEntityDetailFragment();


}
