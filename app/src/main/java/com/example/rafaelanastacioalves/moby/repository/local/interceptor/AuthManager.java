package com.example.rafaelanastacioalves.moby.repository.local.interceptor;

public class AuthManager {

    public String accessToken = "";
    public String client = "";
    public String uid = "";


    private static AuthManager INSTANCE;


    public static AuthManager getInstance(){
        if (INSTANCE == null){
            INSTANCE = new AuthManager();
        }
        return INSTANCE;
    }



}
