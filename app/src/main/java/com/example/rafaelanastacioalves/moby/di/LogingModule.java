package com.example.rafaelanastacioalves.moby.di;

import com.example.rafaelanastacioalves.moby.login.LoginActivity;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class LogingModule {

    @ContributesAndroidInjector(modules = com.example.rafaelanastacioalves.moby.login.LoginModule.class)
    abstract LoginActivity bindLoginActivity();




}
