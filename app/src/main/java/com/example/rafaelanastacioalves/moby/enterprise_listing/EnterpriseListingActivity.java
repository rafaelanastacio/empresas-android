package com.example.rafaelanastacioalves.moby.enterprise_listing;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.example.rafaelanastacioalves.moby.R;
import com.example.rafaelanastacioalves.moby.domain.model.Enterprise;
import com.example.rafaelanastacioalves.moby.domain.model.Enterprises;
import com.example.rafaelanastacioalves.moby.domain.model.Resource;
import com.example.rafaelanastacioalves.moby.enterprise_detailing.EnterpriseDetailActivity;
import com.example.rafaelanastacioalves.moby.enterprise_detailing.EnterpriseDetailsFragment;
import com.example.rafaelanastacioalves.moby.listeners.RecyclerViewClickListener;

import java.util.List;

import javax.inject.Inject;

import dagger.android.AndroidInjection;
import timber.log.Timber;

public class EnterpriseListingActivity extends AppCompatActivity implements RecyclerViewClickListener {
    private final RecyclerViewClickListener mClickListener = this;
    private EnterpriseAdapter mTripPackageListAdapter;
    private RecyclerView mRecyclerView;
    private LiveDataMainEnterpriseListViewModel mLiveDataMainEnterpriseListViewModel;

    @Inject
    EnterpriseListingViewModelFactory projectViewModelFactory;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setupDagger();
        super.onCreate(savedInstanceState);
        setupViews();
        setupRecyclerView();
        subscribe();

    }

    private void setupDagger() {
        AndroidInjection.inject(this);
    }


    private void subscribe() {
        mLiveDataMainEnterpriseListViewModel = ViewModelProviders.of(this, projectViewModelFactory).get(LiveDataMainEnterpriseListViewModel.class);
        mLiveDataMainEnterpriseListViewModel.getMainEntityList().observe(this, new Observer<Resource<Enterprises>>() {
            @Override
            public void onChanged(@Nullable Resource<Enterprises> listResource) {
                Timber.d("On Changed");
                if (listResource != null) {
                    switch (listResource.status) {
                        case INTERNAL_SERVER_ERROR:
                            hideLoading();
                            showErrorView();
                            break;
                        case GENERIC_ERROR:
                            Timber.d("Generic Error");
                            hideLoading();
                            showErrorView();
                            break;
                        case LOADING:
                            showLoading();
                            break;
                        case SUCCESS:
                            Timber.d("Success");
                            hideLoading();
                            populateRecyclerView(listResource.data.enterprises);
                            break;

                    }
                }
            }


        });
    }

    private void hideLoading() {

    }

    private void showLoading() {

    }



    private void showErrorView() {
        //TODO: implement error view
    }

    private void setupViews() {
        setContentView(R.layout.activity_enterprise_listing);
        Timber.tag("LifeCycles");
        Timber.i("onCreate Activity");
    }

    private void setupRecyclerView() {
        mRecyclerView = (RecyclerView) findViewById(R.id.trip_package_list);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        mRecyclerView.setLayoutManager(layoutManager);
        if (mTripPackageListAdapter == null) {
            mTripPackageListAdapter = new EnterpriseAdapter(this);
        }
        mTripPackageListAdapter.setRecyclerViewClickListener(mClickListener);
        mRecyclerView.setAdapter(mTripPackageListAdapter);
    }


    private void populateRecyclerView(List<Enterprise> data) {
        if (data == null) {
            mTripPackageListAdapter.setItems(null);
            //TODO add any error managing
            Timber.w("Nothing returned from Trip Package List API");

        } else {
            mTripPackageListAdapter.setItems(data);
        }

    }


    @Override
    public void onClick(View view, int position) {
        Enterprise Enterprise = (Enterprise) mTripPackageListAdapter.getItems().get(position);

        AppCompatImageView transitionImageView = view.findViewById(R.id.main_entity_imageview);
        startActivityByVersion(Enterprise, transitionImageView);


    }

    private void startActivityByVersion(Enterprise enterprise, AppCompatImageView transitionImageView) {
        Intent i = new Intent(this, EnterpriseDetailActivity.class);
        i.putExtra(EnterpriseDetailsFragment.ENTERPRISE_ID, enterprise.id);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Bundle bundle = null;
            bundle = ActivityOptionsCompat.makeSceneTransitionAnimation(EnterpriseListingActivity.this,
                    transitionImageView, transitionImageView.getTransitionName()).toBundle();
            startActivity(i, bundle);

        } else {
            startActivity(i);
        }
    }
}
