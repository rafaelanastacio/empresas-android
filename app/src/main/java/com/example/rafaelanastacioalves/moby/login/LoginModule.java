package com.example.rafaelanastacioalves.moby.login;

import com.example.rafaelanastacioalves.moby.domain.interactors.LoginInteractor;
import com.example.rafaelanastacioalves.moby.domain.interactors.MainEntityListInteractor;

import dagger.Module;
import dagger.Provides;

@Module
public class LoginModule {


    @Provides
    LoginViewModelFactory projectViewModelFactory(LoginInteractor loginInteractor){
        return new LoginViewModelFactory(loginInteractor);
    }

}
