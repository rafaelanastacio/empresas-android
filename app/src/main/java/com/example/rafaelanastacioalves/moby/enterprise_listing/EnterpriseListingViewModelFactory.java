package com.example.rafaelanastacioalves.moby.enterprise_listing;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;
import android.support.annotation.NonNull;

import com.example.rafaelanastacioalves.moby.domain.interactors.MainEntityListInteractor;


class EnterpriseListingViewModelFactory implements ViewModelProvider.Factory {

    private final MainEntityListInteractor mInteractor;

    public EnterpriseListingViewModelFactory(MainEntityListInteractor interactor){
        this.mInteractor = interactor;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {

        if (modelClass.isAssignableFrom(LiveDataMainEnterpriseListViewModel.class)){
            return (T) new LiveDataMainEnterpriseListViewModel(mInteractor);
        }
        throw new IllegalArgumentException("Unknown ViewModel class");
    }


}
