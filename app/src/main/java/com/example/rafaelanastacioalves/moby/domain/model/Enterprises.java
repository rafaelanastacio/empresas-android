package com.example.rafaelanastacioalves.moby.domain.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Enterprises {

    @SerializedName("enterprises")
    public List<Enterprise> enterprises;
}
