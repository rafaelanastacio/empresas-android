package com.example.rafaelanastacioalves.moby.enterprise_listing;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;

import com.example.rafaelanastacioalves.moby.domain.model.Enterprise;
import com.example.rafaelanastacioalves.moby.domain.model.Enterprises;
import com.example.rafaelanastacioalves.moby.domain.model.Resource;
import com.example.rafaelanastacioalves.moby.domain.interactors.MainEntityListInteractor;

import java.util.List;

public class LiveDataMainEnterpriseListViewModel extends ViewModel {

    private MainEntityListInteractor mMainEntityListinInteractor;
    private LiveData<Resource<Enterprises>> mMainEntityList;


    public LiveDataMainEnterpriseListViewModel(MainEntityListInteractor interactor){
        this.mMainEntityListinInteractor = interactor;
    }

    public LiveData<Resource<Enterprises>> getMainEntityList() {
        mMainEntityList = mMainEntityListinInteractor.execute(null);
        return mMainEntityList;
    }

}
