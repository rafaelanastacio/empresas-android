package com.example.rafaelanastacioalves.moby.login;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.rafaelanastacioalves.moby.R;
import com.example.rafaelanastacioalves.moby.domain.model.LoginResponse;
import com.example.rafaelanastacioalves.moby.domain.model.Resource;
import com.example.rafaelanastacioalves.moby.enterprise_listing.EnterpriseListingActivity;


import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import dagger.android.AndroidInjection;
import timber.log.Timber;

public class LoginActivity extends AppCompatActivity {

    private LiveDataLoginViewModel mLiveDataLoginViewModel;

    @BindView(R.id.email_edit_text)
    EditText emailEditText;

    @BindView(R.id.password_edit_text)
    EditText passWordEditText;

    @BindView(R.id.enter)
    Button buttonEnter;

    @Inject
    LoginViewModelFactory projectViewModelFactory;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setupDagger();
        super.onCreate(savedInstanceState);
        setupViews();
        setupListeners();


    }

    private void setupDagger() {
        AndroidInjection.inject(this);
    }

    private void setupListeners() {
        mLiveDataLoginViewModel = ViewModelProviders.of(this, projectViewModelFactory).get(LiveDataLoginViewModel.class);
        buttonEnter.setOnClickListener(v -> login());
    }

    private void redirectToEnTerpriseListing() {
        Intent i = new Intent(this, EnterpriseListingActivity.class);
        startActivity(i);
    }

    private void hideLoading() {

    }

    private void showLoading() {

    }

    private void showErrorView() {
        //TODO: implement error view
    }

    private void setupViews() {
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        Timber.tag("LifeCycles");
        Timber.i("onCreate Activity");
    }

    private void login() {
        mLiveDataLoginViewModel.login(
                emailEditText.getText().toString(),
                passWordEditText.getText().toString()
        ).observe(this, new Observer<Resource<LoginResponse>>() {
            @Override
            public void onChanged(@Nullable Resource<LoginResponse> listResource) {
                Timber.d("On Changed");
                if (listResource != null) {
                    switch (listResource.status) {
                        case INTERNAL_SERVER_ERROR:
                            hideLoading();
                            showErrorView();
                            break;
                        case GENERIC_ERROR:
                            Timber.d("Generic Error");
                            hideLoading();
                            showErrorView();
                            break;
                        case LOADING:
                            showLoading();
                            break;
                        case UNAUTHORIZED:
                            Toast.makeText(LoginActivity.this, "Credenciais Inválidas!", Toast.LENGTH_LONG).show();
                        case SUCCESS:
                            Timber.d("Success");
                            hideLoading();
                            Toast.makeText(LoginActivity.this, "logado!", Toast.LENGTH_SHORT).show();
                            redirectToEnTerpriseListing();
                            break;

                    }
                }
            }


        });
    }

}
