package com.example.rafaelanastacioalves.moby.di;

import android.content.Context;

import com.example.rafaelanastacioalves.moby.application.EnterpriseApp;
import com.example.rafaelanastacioalves.moby.repository.AppRepository;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class ApplicationModule {

    @Provides
    Context provideContext(EnterpriseApp application) {
        return application.getApplicationContext();
    }

    @Singleton
    @Provides
    AppRepository provideAppRepository(){
        return new AppRepository();
    }

}
