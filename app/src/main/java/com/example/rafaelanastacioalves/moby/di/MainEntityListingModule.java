package com.example.rafaelanastacioalves.moby.di;

import com.example.rafaelanastacioalves.moby.enterprise_listing.EnterpriseMainModule;
import com.example.rafaelanastacioalves.moby.enterprise_listing.EnterpriseListingActivity;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class MainEntityListingModule {

    @ContributesAndroidInjector(modules = EnterpriseMainModule.class)
    abstract EnterpriseListingActivity bindMainActivity();




}
